#include <iostream>
#include <string.h>
#include <XPLMDefs.h>
#include <XPLMUtilities.h>
#include <XPLMInstance.h>
#include <XPLMPlugin.h>
#include <XPLMDataAccess.h>
#include <XPLMProcessing.h>
#include "flightcontrolunit.h"

/* global data here */
static XPLMFlightLoopID gBeforeMainLoop = NULL;
static FlightcontrolUnit *gFlightcontrolUnit = NULL;


/* callback prototypes */
float FlightLoopCallback(
    float inElLastCall,
    float inELTimLastFlightloop,
    int counter, void *inRefcon
);

float EndFlaresCallback(
    float inElLastCall,
    float inELTimLastFlightloop,
    int counter, void *inRefcon
);


PLUGIN_API int XPluginStart(char *outName, char * outSig, char *outDesc) {
    // register plugin
    strcpy(outName, "Flightcontrol system");
    strcpy(outSig, "eu.mwagner24.flightcontrol");
    strcpy(outDesc, "Flight by wire like system");
    // register other stuff
    std::cout << "===== starting flightcontrols =====" << std::endl;
    return 1;
}

PLUGIN_API void xPloginStop(void) {
}

PLUGIN_API int XPluginEnable(void) {
    std::cout << "===== enabling flightcontrol plugin =====" << std::endl;
    try {
        gFlightcontrolUnit = new FlightcontrolUnit();
    }
    catch(int e) {
        std::cout << "===== could not enable flightcontrol plugin =====" << std::endl;
        return 0;
    }
    XPLMCreateFlightLoop_t loop_params;
    loop_params.structSize = sizeof(XPLMCreateFlightLoop_t);
    loop_params.phase = xplm_FlightLoop_Phase_BeforeFlightModel;
    loop_params.callbackFunc = FlightLoopCallback;
    gBeforeMainLoop = XPLMCreateFlightLoop(&loop_params);
    if(gBeforeMainLoop == NULL) {
        std::cout << "======================================" << std::endl;
    }
    XPLMScheduleFlightLoop(gBeforeMainLoop,-1.0,0);
    return 1;
}

PLUGIN_API void XPluginDisable(void) {
    XPLMDestroyFlightLoop(gBeforeMainLoop);
    delete gFlightcontrolUnit;
}

float FlightLoopCallback(
    float inElLastCall, 
    float inELTimLastFlightloop,
    int counter, void *inRefcon
) {
    return gFlightcontrolUnit->flightloop(inElLastCall, inELTimLastFlightloop, counter, inRefcon);
}