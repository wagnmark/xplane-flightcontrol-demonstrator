#ifndef __FLIGHTCONTROLLER_H__
#define __FLIGHTCONTROLLER_H__

class Flightcontroller {
private:
    float p;
    float i;
    float d;
    float control_output;
    double integrator;
    float last_err;
public:
    Flightcontroller(float p, float i, float d);
    float loop_callback(float delta_t, float error_val, float speed_compensation_factor);
    float get_output();
    ~Flightcontroller();
};

#endif