#include "flightcontrolunit.h"
#include <iostream>

FlightcontrolUnit::FlightcontrolUnit() {
    aileron1 = XPLMFindDataRef("sim/flightmodel2/wing/aileron1_deg");
    airspeed = XPLMFindDataRef("sim/cockpit2/gauges/indicators/airspeed_kts_pilot");
    angle_of_attack_deg = XPLMFindDataRef("sim/flightmodel2/misc/AoA_angle_degrees");
    elevator1 = XPLMFindDataRef("sim/flightmodel2/wing/elevator1_deg");
    override_control_surf = XPLMFindDataRef("sim/operation/override/override_control_surfaces");
    sideslip = XPLMFindDataRef("sim/cockpit2/gauges/indicators/slip_deg");
    pitch = XPLMFindDataRef("sim/cockpit/gyros/the_ind_deg3");
    roll = XPLMFindDataRef("sim/cockpit/gyros/phi_ind_deg3");
    rudder1 = XPLMFindDataRef("sim/flightmodel2/wing/rudder1_deg");
    yoke_pitch = XPLMFindDataRef("sim/joystick/yoke_pitch_ratio");
    yoke_roll = XPLMFindDataRef("sim/joystick/yoke_roll_ratio");
    last_pitch_deg = 0.0;
    last_roll_deg = 0.0;
    if(
        aileron1 == nullptr ||
        airspeed == nullptr ||
        angle_of_attack_deg == nullptr ||
        elevator1 == nullptr ||
        override_control_surf == nullptr ||
        sideslip == nullptr ||
        pitch == nullptr ||
        roll == nullptr ||
        rudder1 == nullptr ||
        yoke_pitch == nullptr ||
        yoke_roll == nullptr
    ) {
        throw 1;
    }
    XPLMSetDatai(override_control_surf, 1);
    //pitchcontroller = new Flightcontroller(3.0,1.08,0.08);
    //rollcontroller = new Flightcontroller(20.0,20.0,0.1);
    pitchcontroller = new Flightcontroller(20.0,20.0,0.1);
    rollcontroller = new Flightcontroller(3.0,1.08, 0.08);

}

float FlightcontrolUnit::flightloop(
    float inElLastCall,
    float inELTimLastFlightloop,
    int counter,
    void *inRefcon
){
    float airspeed_val = XPLMGetDataf(airspeed);
    float aoa_deg = XPLMGetDataf(angle_of_attack_deg);
    float comp_factor = 1;
    if(airspeed_val > 1.0)
        comp_factor = 100 / (airspeed_val * airspeed_val);
    float pitch_deg = XPLMGetDataf(pitch);
    float roll_deg = XPLMGetDataf(roll);
    float pitch_rate = (pitch_deg - last_pitch_deg) / inElLastCall;
    float roll_rate = (roll_deg - last_roll_deg) / inElLastCall;
    float pitch_rate_goal = XPLMGetDataf(yoke_pitch);
    float roll_rate_goal = XPLMGetDataf(yoke_roll);
    if(aoa_deg > 10.0) {
        pitch_rate_goal = -0.5 * (aoa_deg - 10);
    } else if (aoa_deg < -10) {
        pitch_rate_goal = -0.5 * (aoa_deg + 10);
    }
    float aileron1_left_val = 0.0;
    float aileron1_right_val = 0.0;
    float elevator1_left_val = 0.0;
    float elevator1_right_val = 0.0;
    float rudder1_val = 0.0;
    // apply controller
    if(airspeed_val >= 50.0) {
        float roll_out = rollcontroller->loop_callback(
            inElLastCall,
            10 * roll_rate_goal - roll_rate,
            comp_factor
        );
        aileron1_left_val = 20 * roll_out;
        aileron1_right_val= -20 * roll_out;
    } else {
        aileron1_left_val = 20 * roll_rate_goal;
        aileron1_right_val = -20 * roll_rate_goal;
    }
    if(airspeed_val > 60) {
        float pitch_out = pitchcontroller->loop_callback(
            inElLastCall,
            5 * pitch_rate_goal - pitch_rate,
            comp_factor
        );
        elevator1_left_val = -10 * pitch_out;
        elevator1_right_val = -10 * pitch_out;
    } else {
        elevator1_left_val = -10 * pitch_rate_goal;
        elevator1_right_val = -10 * pitch_rate_goal;
    }
    XPLMSetDatavf(aileron1, &aileron1_left_val, 0, 1);
    XPLMSetDatavf(aileron1, &aileron1_right_val, 1, 1);
    XPLMSetDatavf(elevator1, &elevator1_left_val, 8, 1);
    XPLMSetDatavf(elevator1, &elevator1_right_val, 9, 1);
    XPLMSetDatavf(rudder1, &rudder1_val, 0, 1);
    last_pitch_deg = pitch_deg;
    last_roll_deg = roll_deg;
    std::cout << "airspeed:    " << airspeed_val << std::endl;
    std::cout << "target roll: " << roll_rate_goal << std::endl;
    std::cout << "rollrate     " << roll_rate << std::endl;
    std::cout << "pitchrate    " << pitch_rate << std::endl;
    std::cout << "roll         " << roll_deg << std::endl;
    std::cout << "pitch        " << pitch_deg << std::endl;
    std::cout << "el time      " << inElLastCall << std::endl;
    return -1;
};

FlightcontrolUnit::~FlightcontrolUnit() {
    delete pitchcontroller;
    delete rollcontroller;
    XPLMSetDatai(override_control_surf, 0);
}