#ifndef __FLIGHTCONTROLUNIT_H__
#define __FLIGHTCONTROLUNIT_H__

#include "flightcontroller.h"
#include <XPLMDataAccess.h>

class FlightcontrolUnit {
private:
    XPLMDataRef aileron1;
    XPLMDataRef airspeed;
    XPLMDataRef angle_of_attack_deg;
    XPLMDataRef elevator1;
    XPLMDataRef override_control_surf;
    XPLMDataRef sideslip;
    XPLMDataRef pitch;
    XPLMDataRef roll;
    XPLMDataRef rudder1;
    XPLMDataRef yoke_pitch;
    XPLMDataRef yoke_roll;
    Flightcontroller *pitchcontroller;
    Flightcontroller *rollcontroller;
    float last_err;
    float last_pitch_deg;
    float last_roll_deg;
public:
    FlightcontrolUnit();
    float flightloop(
        float inElLastCall,
        float inELTimLastFlightloop,
        int counter,
        void *inRefcon
    );
    ~FlightcontrolUnit();
};

#endif