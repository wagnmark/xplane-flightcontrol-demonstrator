#include "flightcontroller.h"

Flightcontroller::Flightcontroller(float p, float i, float d) {
    this->p = p;
    this->i = i;
    this->d = d;
    integrator = 0.0;
    last_err = 0.0;
    control_output = 0.0;
}

Flightcontroller::~Flightcontroller() {
}

float Flightcontroller::loop_callback(float delta_t, float error_val, float speed_compensation_factor) {
    float differentiator = (error_val - last_err) / delta_t;
    control_output = speed_compensation_factor * (
        p * error_val +
        i * integrator +
        d * differentiator
    );
    if(control_output < -1.0) {
        control_output = -1.0;
        if(error_val > 0)
            integrator += error_val * delta_t;
    } else if(control_output > 1.0) {
        control_output = 1.0;
        if(error_val < 0)
            integrator += error_val * delta_t;
    } else {
        integrator += error_val * delta_t;
    }
    return control_output;
};

float Flightcontroller::get_output() {
    return control_output;
};
